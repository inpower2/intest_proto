defmodule IntestProto do
  @moduledoc """
  Documentation for `IntestProto`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> IntestProto.hello()
      :world

  """
  def hello do
    :world
  end
end
