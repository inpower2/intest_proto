defmodule InpowerTest.GetAttemptRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          service_id: String.t(),
          service_type: String.t(),
          user_id: String.t()
        }

  defstruct [:service_id, :service_type, :user_id]

  field :service_id, 1, type: :string
  field :service_type, 2, type: :string
  field :user_id, 3, type: :string
end

defmodule InpowerTest.GetAttemptsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          service_id: String.t(),
          service_type: String.t(),
          user_id: String.t()
        }

  defstruct [:service_id, :service_type, :user_id]

  field :service_id, 1, type: :string
  field :service_type, 2, type: :string
  field :user_id, 3, type: :string
end

defmodule InpowerTest.GetQuestionsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          service_id: String.t(),
          service_type: String.t()
        }

  defstruct [:service_id, :service_type]

  field :service_id, 1, type: :string
  field :service_type, 2, type: :string
end

defmodule InpowerTest.CreateQuestionRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          service_id: String.t(),
          service_type: String.t(),
          question: String.t(),
          description: String.t(),
          type: String.t()
        }

  defstruct [:service_id, :service_type, :question, :description, :type]

  field :service_id, 1, type: :string
  field :service_type, 2, type: :string
  field :question, 3, type: :string
  field :description, 4, type: :string
  field :type, 5, type: :string
end

defmodule InpowerTest.EditQuestionRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer,
          service_id: String.t(),
          service_type: String.t(),
          question: String.t(),
          description: String.t(),
          type: String.t()
        }

  defstruct [:id, :service_id, :service_type, :question, :description, :type]

  field :id, 1, type: :int32
  field :service_id, 2, type: :string
  field :service_type, 3, type: :string
  field :question, 4, type: :string
  field :description, 5, type: :string
  field :type, 6, type: :string
end

defmodule InpowerTest.AddOptionsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          question_id: integer,
          options: [InpowerTest.Option.t()]
        }

  defstruct [:question_id, :options]

  field :question_id, 1, type: :int32
  field :options, 2, repeated: true, type: InpowerTest.Option
end

defmodule InpowerTest.EditOptionsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          question_id: integer,
          options: [InpowerTest.Option.t()]
        }

  defstruct [:question_id, :options]

  field :question_id, 1, type: :int32
  field :options, 2, repeated: true, type: InpowerTest.Option
end

defmodule InpowerTest.OptionsRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          question_id: integer,
          options: [InpowerTest.Option.t()]
        }

  defstruct [:question_id, :options]

  field :question_id, 1, type: :int32
  field :options, 2, repeated: true, type: InpowerTest.Option
end

defmodule InpowerTest.GetQuestionsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          questions: [InpowerTest.QuestionWithOptions.t()]
        }

  defstruct [:questions]

  field :questions, 1, repeated: true, type: InpowerTest.QuestionWithOptions
end

defmodule InpowerTest.CreateQuestionResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          question: InpowerTest.Question.t() | nil
        }

  defstruct [:question]

  field :question, 1, type: InpowerTest.Question
end

defmodule InpowerTest.EditQuestionResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          question: InpowerTest.Question.t() | nil
        }

  defstruct [:question]

  field :question, 1, type: InpowerTest.Question
end

defmodule InpowerTest.AddOptionsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          options: [InpowerTest.Option.t()]
        }

  defstruct [:options]

  field :options, 2, repeated: true, type: InpowerTest.Option
end

defmodule InpowerTest.EditOptionsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          options: [InpowerTest.Option.t()]
        }

  defstruct [:options]

  field :options, 1, repeated: true, type: InpowerTest.Option
end

defmodule InpowerTest.GetAttemptsResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          attempts: [InpowerTest.Attempt.t()]
        }

  defstruct [:attempts]

  field :attempts, 1, repeated: true, type: InpowerTest.Attempt
end

defmodule InpowerTest.GetAttemptResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          attempt: InpowerTest.Attempt.t() | nil
        }

  defstruct [:attempt]

  field :attempt, 1, type: InpowerTest.Attempt
end

defmodule InpowerTest.Option do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          question_id: integer,
          option: String.t(),
          correct: boolean,
          id: integer
        }

  defstruct [:question_id, :option, :correct, :id]

  field :question_id, 1, type: :int32
  field :option, 2, type: :string
  field :correct, 3, type: :bool
  field :id, 4, type: :int32
end

defmodule InpowerTest.OptionWithQuestion do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          question_id: integer,
          option: String.t(),
          correct: boolean,
          id: integer,
          question: InpowerTest.Question.t() | nil
        }

  defstruct [:question_id, :option, :correct, :id, :question]

  field :question_id, 1, type: :int32
  field :option, 2, type: :string
  field :correct, 3, type: :bool
  field :id, 4, type: :int32
  field :question, 5, type: InpowerTest.Question
end

defmodule InpowerTest.QuestionWithOptions do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer,
          question: String.t(),
          service_id: String.t(),
          service_type: String.t(),
          type: String.t(),
          options: [InpowerTest.Option.t()],
          description: String.t()
        }

  defstruct [:id, :question, :service_id, :service_type, :type, :options, :description]

  field :id, 1, type: :int32
  field :question, 2, type: :string
  field :service_id, 3, type: :string
  field :service_type, 4, type: :string
  field :type, 5, type: :string
  field :options, 6, repeated: true, type: InpowerTest.Option
  field :description, 7, type: :string
end

defmodule InpowerTest.Question do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer,
          question: String.t(),
          service_id: String.t(),
          service_type: String.t(),
          type: String.t(),
          description: String.t()
        }

  defstruct [:id, :question, :service_id, :service_type, :type, :description]

  field :id, 1, type: :int32
  field :question, 2, type: :string
  field :service_id, 3, type: :string
  field :service_type, 4, type: :string
  field :type, 5, type: :string
  field :description, 6, type: :string
end

defmodule InpowerTest.MakeAttemptRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          user_id: String.t(),
          service_id: String.t(),
          service_type: String.t(),
          answers: [InpowerTest.AnswerRequest.t()]
        }

  defstruct [:user_id, :service_id, :service_type, :answers]

  field :user_id, 1, type: :string
  field :service_id, 2, type: :string
  field :service_type, 3, type: :string
  field :answers, 4, repeated: true, type: InpowerTest.AnswerRequest
end

defmodule InpowerTest.AnswerRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          question_id: integer,
          options: [InpowerTest.OptionRequest.t()]
        }

  defstruct [:question_id, :options]

  field :question_id, 2, type: :int32
  field :options, 3, repeated: true, type: InpowerTest.OptionRequest
end

defmodule InpowerTest.OptionRequest do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer
        }

  defstruct [:id]

  field :id, 1, type: :int32
end

defmodule InpowerTest.OptionResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer,
          option: String.t()
        }

  defstruct [:id, :option]

  field :id, 1, type: :int32
  field :option, 2, type: :string
end

defmodule InpowerTest.MakeAttemptResponse do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          attempt: InpowerTest.Attempt.t() | nil
        }

  defstruct [:attempt]

  field :attempt, 1, type: InpowerTest.Attempt
end

defmodule InpowerTest.Attempt do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer,
          score_percent: float | :infinity | :negative_infinity | :nan,
          score: float | :infinity | :negative_infinity | :nan,
          service_type: String.t(),
          user: String.t(),
          answers: [InpowerTest.Answer.t()],
          service_id: String.t()
        }

  defstruct [:id, :score_percent, :score, :service_type, :user, :answers, :service_id]

  field :id, 1, type: :int32
  field :score_percent, 2, type: :float
  field :score, 3, type: :float
  field :service_type, 4, type: :string
  field :user, 5, type: :string
  field :answers, 6, repeated: true, type: InpowerTest.Answer
  field :service_id, 7, type: :string
end

defmodule InpowerTest.Answer do
  @moduledoc false
  use Protobuf, syntax: :proto3

  @type t :: %__MODULE__{
          id: integer,
          question_id: integer,
          score: String.t(),
          options: [InpowerTest.OptionResponse.t()],
          question: InpowerTest.QuestionWithOptions.t() | nil,
          attempt_id: String.t(),
          make: float | :infinity | :negative_infinity | :nan
        }

  defstruct [:id, :question_id, :score, :options, :question, :attempt_id, :make]

  field :id, 1, type: :int32
  field :question_id, 2, type: :int32
  field :score, 3, type: :string
  field :options, 4, repeated: true, type: InpowerTest.OptionResponse
  field :question, 5, type: InpowerTest.QuestionWithOptions
  field :attempt_id, 6, type: :string
  field :make, 7, type: :float
end

defmodule InpowerTest.TestService.Service do
  @moduledoc false
  use GRPC.Service, name: "inpower_test.TestService"

  rpc :GetQuestions, InpowerTest.GetQuestionsRequest, InpowerTest.GetQuestionsResponse

  rpc :CreateQuestion, InpowerTest.CreateQuestionRequest, InpowerTest.CreateQuestionResponse

  rpc :EditQuestion, InpowerTest.EditQuestionRequest, InpowerTest.EditQuestionResponse

  rpc :CreateOptions, InpowerTest.AddOptionsRequest, InpowerTest.AddOptionsResponse

  rpc :EditOptions, InpowerTest.EditOptionsRequest, InpowerTest.EditOptionsResponse

  rpc :MakeAttempt, InpowerTest.MakeAttemptRequest, InpowerTest.MakeAttemptResponse

  rpc :GetAttempts, InpowerTest.GetAttemptsRequest, InpowerTest.GetAttemptsResponse

  rpc :GetAttempt, InpowerTest.GetAttemptRequest, InpowerTest.GetAttemptResponse
end

defmodule InpowerTest.TestService.Stub do
  @moduledoc false
  use GRPC.Stub, service: InpowerTest.TestService.Service
end
